package com.example.lessson17;

import entity.Book;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import service.BookService;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "ShowBooks", value = "/ShowBooks")
public class ShowBook extends HttpServlet {
    BookService bookService = new BookService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Book> books = bookService.findAll();

        req.setAttribute("books", books);
        //:Чтобы не завершать процесс пустым листом, вывожу ошибку на экран пользователя
        if (books.isEmpty()){
            getServletContext().getRequestDispatcher("/error.jsp").forward(req, resp);
        }
        else   getServletContext().getRequestDispatcher("/listOfBooks.jsp").forward(req, resp);
    }
}
