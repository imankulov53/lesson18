package com.example.lessson17;

import entity.Book;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import service.BookService;

import java.io.IOException;
import java.util.List;


@WebServlet(name = "/putBooks", value = "/putBooks")
public class HelloServlet extends HttpServlet {
    BookService bookService = new BookService();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(req.getMethod());
        String name = req.getParameter("name");
        String author = req.getParameter("author");
        String publisher = req.getParameter("publisher");
        int publishingYear = Integer.parseInt(req.getParameter("year"));

        Book book = new Book();

        book.setName(name);
        book.setAuthor(author);
        book.setPublisher(publisher);
        book.setYear(publishingYear);
        bookService.persist(book);

        //:Возвращаюсь обратно на стартовую страницу, потому что добавил кнопку которая может вывести список книг.
        getServletContext().getRequestDispatcher("/index.jsp").forward(req, resp);

    }

}