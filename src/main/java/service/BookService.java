package service;

import dao.BookDao;
import dao.Dao;
import entity.Book;

import java.util.List;

public class BookService {

    private static Dao<Book,Integer> bookDao;

    public BookService() {
        bookDao = new BookDao();
    }

    public void persist(Book client){
        bookDao.openSession();
        bookDao.persist(client);
        bookDao.closeSession();
    }

    public void update(Book client){
        bookDao.openSession();
        bookDao.update(client);
        bookDao.closeSession();
    }

    public Book findById(Integer integer){
        bookDao.openSession();
        Book client = bookDao.findById(integer);
        bookDao.closeSession();
        return client;
    }

    public void delete(Integer integer){
        bookDao.openSession();
        bookDao.delete(findById(integer));
        bookDao.closeSession();
    }

    public List<Book> findAll(){
        bookDao.openSession();
        List<Book> list = bookDao.findAll();
        bookDao.closeSession();
        return list;
    }

    public void deleteAll(){
        bookDao.openSession();
        bookDao.deleteAll();
        bookDao.closeSession();
    }
}
