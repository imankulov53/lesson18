package dao;

import entity.Book;

import java.util.List;

public class BookDao extends OpenDao<Book,Integer>{
    @Override
    public void persist(Book entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Book entity) {
        getCurrentSession().saveOrUpdate(entity);
    }

    @Override
    public Book findById(Integer integer) {
        return getCurrentSession().get(Book.class,integer);
    }

    @Override
    public void delete(Book entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<Book> findAll() {
        return getCurrentSession().createQuery("FROM Book").list();
    }

    @Override
    public void deleteAll() {
        findAll().forEach(client -> delete(client));
    }
}
