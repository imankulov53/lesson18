package entity;

import javax.persistence.*;

@Entity
@Table(name= "BOOK")
public class Book {
    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "id_book", sequenceName = "id_book", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_book")
    int id;

    @Column(name ="name")
    String name;

    @Column(name = "author")
    String author;

    @Column(name = "publisher")
    String publisher;

    @Column(name ="year")
    int year;

    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }
    public Book(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
