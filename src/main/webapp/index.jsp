<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<h1><%= "Hello World!" %>
</h1>
<br/>
<%--<a href="hello-servlet">Hello Servlet</a>--%>

<form action="putBooks" method="POST" >
    Name of book: <input name="name"/>
    <br><br>

    Author: <input name="author"/>
    <br></br>

    Publisher: <input name="publisher"/>
    <br></br>

    Publication year: <input name="year" type="number" min=1/>
    <br></br>

    <input type="submit" value="Put Books"/>
</form>
<form action="ShowBooks" method="GET">
    <button type="submit"> Show Books </button>
</form>
</body>
</html>