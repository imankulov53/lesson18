<%@ page import="entity.Book" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Locale" %>

<%--&lt;%&ndash;--%>
<%--  Created by IntelliJ IDEA.--%>
<%--  User: User--%>
<%--  Date: 13.04.2021--%>
<%--  Time: 21:39--%>
<%--  To change this template use File | Settings | File Templates.--%>
<%--&ndash;%&gt;--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>List of Books</title>
</head>
<body>
<%
    List<Book> books = (List) request.getAttribute("books");
%>
<h1>Books</h1>
<table style="width:100%" border="1">
    <tr>
        <th>Name of Book</th>
        <th>Author</th>
        <th>Publisher</th>
        <th>Publishing year</th>
    </tr>
    <%for ( Book book : books){ %>
    <tr>
        <th><%=book.getName()%></th>
        <th><%=book.getAuthor()%></th>
        <th><%=book.getPublisher()%></th>
        <th><%=book.getYear()%></th>
    </tr>
    <%}%>

</table>
</body>
</html>
